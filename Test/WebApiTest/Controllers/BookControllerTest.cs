﻿using AutoMapper;
using Domain;
using Domain.Abstractions;
using Domain.Format;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Controllers;
using WebApi.DataTransferObject;
using WebApi.Mappers;
using Xunit;

namespace WebApiTest.Controllers
{
    public class BookControllerTest
    {
        private Mock<IBookService> bookServiceMock;
        private IMapper mapper;
        private BookController bookController;
        public BookControllerTest()
        {
            this.bookServiceMock = new Mock<IBookService>();

            BookProfile profile = new BookProfile();
            MapperConfiguration configurationMapper = new MapperConfiguration(cfg => cfg.AddProfile(profile));
            this.mapper = new Mapper(configurationMapper);

            bookController = new BookController(bookServiceMock.Object, mapper);
        }

        [Fact]
        public async Task CreateBookAsync_ReturnCreatedCode_OnGivenBookDTO()
        {
            BookForCreation bookForCreation = new BookForCreation
            {
                Title = "The Castle",
                Subtitle = "",
                ISBN = "1254521254514",
                PublisherName = "string",
                PublishedYear = 2012,
                Price = 100
            };

            bookServiceMock
                .Setup(service => service.CreateBookAsync(It.IsAny<Book>()))
                .Callback((Book book) =>
                {
                    book.Id = Guid.NewGuid();
                });

            Assert.IsType<ObjectResult>(await bookController.CreateBookAsync(bookForCreation));
        }


        [Fact]
        public async Task UpdateBookAsync_ReturnCreatedCode_OnGivenBookDTO()
        {
            BookForUpdate bookForUpdate = new BookForUpdate
            {
                Id = Guid.NewGuid(),
                Title = "The Castle 2",
                Subtitle = "",
                ISBN = "1254521254514",
                PublisherName = "string",
                PublishedYear = 2012,
                Price = 100
            };

            bookServiceMock
                .Setup(service => service.UpdateBookAsync(It.IsAny<Book>()))
                .Callback((Book book) =>
                {
                    book.Title = "The Castle 2";
                });

            bookServiceMock
                .Setup(service => service.GetBookByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() =>
                {
                    return new Book
                    {
                        Id = Guid.NewGuid(),
                        Title = "The Castle 2",
                        Subtitle = "",
                        ISBN = "1254521254514",
                        PublisherName = "string",
                        PublishedYear = 2012,
                        Price = 100
                    };
                });

            Assert.IsType<OkObjectResult>(await bookController.UpdateBookAsync(bookForUpdate));
            bookServiceMock
               .Verify(service => service.UpdateBookAsync(It.IsAny<Book>()), Times.AtLeast(1));
        }


        [Fact]
        public async Task GetBookByIdAsync_ReturnBook_OnGivenBookId()
        {
            Guid id = Guid.NewGuid();
            Book book = new Book
            {
                Id = id,
                Title = "The Castle 2",
                Subtitle = "",
                ISBN = "1254521254514",
                PublisherName = "string",
                PublishedYear = 2012,
                Price = 100
            };

            bookServiceMock
                .Setup(service => service.GetBookByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() =>
                {
                    return book;
                });

            Assert.IsType<OkObjectResult>(await bookController.GetBookByIdAsync(id));
        }


        [Fact]
        public async Task GetBookByIdAsync_ReturnNotFound_OnGivenBookId()
        {
            Guid id = Guid.NewGuid();


            bookServiceMock
                .Setup(service => service.GetBookByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() =>
                {
                    return null;
                });

            Assert.IsType<NotFoundResult>(await bookController.GetBookByIdAsync(id));
        }


        [Fact]
        public async Task GetAllBooksAsync_ReturnOk_OnGivenPageAndSize()
        {

            BookQuery bookQuery = new BookQuery{};


            bookServiceMock
                .Setup(service => service.GetAllBooksAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<int>(), It.IsAny<int>()))
                .ReturnsAsync(() =>
                {
                    return new Paginate<Book>
                    {
                        Content = new List<Book>()
                        {
                            new Book {
                                Id = Guid.NewGuid(),
                                Title = "The Castle 2",
                                Subtitle = "",
                                ISBN = "1254521254514",
                                PublisherName = "string",
                                PublishedYear = 2012,
                                Price = 100
                            },
                            new Book {
                                Id = Guid.NewGuid(),
                                Title = "The Castle 2",
                                Subtitle = "",
                                ISBN = "1254521254514",
                                PublisherName = "string",
                                PublishedYear = 2012,
                                Price = 100
                            }
                        },
                        Total = 2
                    };
                });

            Assert.IsType<OkObjectResult>(await bookController.GetAllBooksAsync(bookQuery));
        }

        [Fact]
        public async Task DeleteBookById_ReturnNoContent_OnGivenBookId()
        {
            Guid id = Guid.NewGuid();
            Assert.IsType<NoContentResult>(await bookController.DeleteBookById(id));
            bookServiceMock
               .Verify(service => service.DeleteBookAsync(It.IsAny<Guid>()), Times.Once);
        }
    }
}
