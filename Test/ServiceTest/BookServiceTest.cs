using Domain;
using Domain.Abstractions;
using Domain.Format;
using Moq;
using Service.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ServiceTest
{
    public class BookServiceTest
    {
        private BookService bookService;
        private Mock<IBookRepository> bookRepositoryMock;

        public BookServiceTest()
        {
            bookRepositoryMock = new Mock<IBookRepository>();
            bookService = new BookService(bookRepositoryMock.Object);
        }

        [Fact]
        public async Task CreateBookAsync_UpdateBookObject_OnGivenBook()
        {
            Book book = new Book
            {
                Title = "The Castle 2",
                Subtitle = "",
                ISBN = "1254521254514",
                PublisherName = "string",
                PublishedYear = 2012,
                Price = 100
            };

            this.bookRepositoryMock.Setup(repository => repository.CreateBookAsync(It.IsAny<Book>()))
                .Callback((Book book) =>
                {
                    book.Id = Guid.NewGuid();
                });

            await bookService.CreateBookAsync(book);

            Assert.NotEqual(new Guid(), book.Id);
            this.bookRepositoryMock.Verify(repository => repository.CreateBookAsync(It.IsAny<Book>()), Times.Once);
        }


        [Fact]
        public async Task UpdateBookAsync_UpdateBookObject_OnGivenBook()
        {
            Book book = new Book
            {
                Title = "The Castle 2",
                Subtitle = "",
                ISBN = "1254521254514",
                PublisherName = "string",
                PublishedYear = 2012,
                Price = 100
            };

            await bookService.UpdateBookAsync(book);

            this.bookRepositoryMock.Verify(repository => repository.UpdateBookAsync(It.IsAny<Book>()), Times.Once);
        }

        [Fact]
        public async Task DeleteBookAsync_OnGivenId()
        {
            await bookService.DeleteBookAsync(Guid.NewGuid());

            this.bookRepositoryMock.Verify(repository => repository.DeleteBookAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public async Task GetBookByIdAsync_ReturnBook_OnGivenBook()
        {
            Guid id = Guid.NewGuid();
            this.bookRepositoryMock.Setup(repository => repository.GetBookByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() =>
                {
                    return new Book
                    {
                        Id = id,
                        Title = "The Castle 2",
                        Subtitle = "",
                        ISBN = "1254521254514",
                        PublisherName = "string",
                        PublishedYear = 2012,
                        Price = 100
                    };
                });

            Assert.IsType<Book>(await bookService.GetBookByIdAsync(id));
            this.bookRepositoryMock.Verify(repository => repository.GetBookByIdAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public async Task GetAllBooksAsync_ReturnListOfBooks_OnGivenSearchParameters()
        {

            string isbn = "";
            string title = "";
            string publisher = "";
            int yearPublished = 2012;
            decimal price = 10.2M;
            int offset = 0;
            int limit = 10;

            bookRepositoryMock
                .Setup(repository => repository.GetAllBooksAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<int>(), It.IsAny<int>()))
                .ReturnsAsync(() =>
                {
                    return new Paginate<Book>
                    {
                        Content = new List<Book>()
                        {
                            new Book {
                                Id = Guid.NewGuid(),
                                Title = "The Castle 2",
                                Subtitle = "",
                                ISBN = "1254521254514",
                                PublisherName = "string",
                                PublishedYear = 2012,
                                Price = 100
                            },
                            new Book {
                                Id = Guid.NewGuid(),
                                Title = "The Castle 2",
                                Subtitle = "",
                                ISBN = "1254521254514",
                                PublisherName = "string",
                                PublishedYear = 2012,
                                Price = 100
                            }
                        },
                        Total = 2
                    };
                });
            

            Assert.IsType<Paginate<Book>>(await bookService.GetAllBooksAsync(isbn, title, publisher, yearPublished, price, offset, limit));
            this.bookRepositoryMock.Verify(repository => repository.GetAllBooksAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<decimal>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
        }
    }
}
