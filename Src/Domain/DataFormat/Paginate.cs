﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Format
{
    public class Paginate<T>
    {
        public List<T> Content { get; set; }
        public int Total { get; set; }
    }
}
