﻿using Domain.Format;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Abstractions
{
    public interface IBookRepository
    {
        Task<Book> GetBookByIdAsync(Guid id);
        Task<Paginate<Book>> GetAllBooksAsync(string isbn,
            string title,
            string publisher,
            int yearPublished,
            decimal price,
            int offset,
            int limit);
        Task CreateBookAsync(Book book);
        Task UpdateBookAsync(Book book);
        Task DeleteBookAsync(Guid id);
    }
}
