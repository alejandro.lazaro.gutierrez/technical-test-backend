﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Book
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string ISBN { get; set; }
        public string PublisherName { get; set; }
        public int PublishedYear { get; set; }
        public decimal Price { get; set; }
    }
}
