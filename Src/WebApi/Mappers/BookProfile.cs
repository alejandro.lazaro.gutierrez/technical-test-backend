﻿using AutoMapper;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.DataTransferObject;

namespace WebApi.Mappers
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<BookForCreation, Book>();
            CreateMap<Book, BookForCreation>();

            CreateMap<BookForUpdate, Book>();
            CreateMap<Book, BookForUpdate>();
        }
    }
}
