﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.DataTransferObject
{
    public class BookQuery
    {
        public string Title { get; set; } = "";
        public string ISBN { get; set; } = "";
        public string PublisherName { get; set; } = "";
        public int PublishedYear { get; set; } = 0;
        public decimal Price { get; set; } = 0; 
        public int Page { get; set; }
        public int Size { get; set; }
    }
}
