﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.DataTransferObject
{
    public class BookForCreation
    {
        [Required]
        public string Title { get; set; }
        public string Subtitle { get; set; }
        [Required]
        public string ISBN { get; set; }
        [Required]
        public string PublisherName { get; set; }
        public int PublishedYear { get; set; }
        [Required]
        [Range(0,1000)]
        public decimal Price { get; set; }
    }
}
