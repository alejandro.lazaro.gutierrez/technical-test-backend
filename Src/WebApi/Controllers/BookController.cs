﻿using AutoMapper;
using Domain;
using Domain.Abstractions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using WebApi.DataTransferObject;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BookController : ControllerBase
    {
        private readonly IBookService bookService;
        private readonly IMapper mapper;

        public BookController(IBookService bookService,IMapper mapper)
        {
            this.bookService = bookService;
            this.mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> CreateBookAsync([FromBody] BookForCreation bookForCreation)
        {
            Book book = mapper.Map<Book>(bookForCreation);
            await bookService.CreateBookAsync(book);
            return new ObjectResult(book) { StatusCode = 201};
        }

        [HttpPut]
        public async Task<IActionResult> UpdateBookAsync([FromBody] BookForUpdate bookForUpdate)
        {
            Book book = mapper.Map<Book>(bookForUpdate);
            Book bookObtained = await bookService.GetBookByIdAsync(book.Id);

            if (bookObtained is null)
            {
                return NotFound();
            }

            await bookService.UpdateBookAsync(book);
            return Ok(book);
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetBookByIdAsync(Guid id)
        {
            Book book = await bookService.GetBookByIdAsync(id);
            
            if(book is null)
            {
                return NotFound();
            }

            return  Ok(book);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllBooksAsync([FromQuery] BookQuery bookQuery)
        {
            return Ok(await bookService.GetAllBooksAsync(bookQuery.ISBN, bookQuery.Title, bookQuery.PublisherName, bookQuery.PublishedYear, bookQuery.Price ,bookQuery.Page, bookQuery.Size));
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteBookById(Guid id)
        {
            await bookService.DeleteBookAsync(id);
            return NoContent();
        }
    }
}
