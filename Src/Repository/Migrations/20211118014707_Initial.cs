﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Repository.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Books",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "BLOB(16)", nullable: false),
                    Title = table.Column<string>(type: "VARCHAR(255)", nullable: false, defaultValue: "0"),
                    Subtitle = table.Column<string>(type: "VARCHAR(200)", nullable: false, defaultValue: "0"),
                    ISBN = table.Column<string>(type: "VARCHAR(40)", nullable: false, defaultValue: "0"),
                    PublisherName = table.Column<string>(type: "VARCHAR(100)", nullable: false, defaultValue: "0"),
                    PublishedYear = table.Column<int>(type: "INTEGER", nullable: false),
                    Price = table.Column<decimal>(type: "DECIMAL(10,5)", nullable: false, defaultValue: 0m)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Books", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Books");
        }
    }
}
