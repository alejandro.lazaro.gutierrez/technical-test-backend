﻿using Domain;
using Domain.Abstractions;
using Domain.Format;
using Microsoft.EntityFrameworkCore;
using Repository.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class BookRepository : IBookRepository
    {
        private readonly ApplicationDbContext applicationDbContext;

        public BookRepository(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public async Task CreateBookAsync(Book book)
        {
            await applicationDbContext
                .Books
                .AddAsync(book);
            await applicationDbContext.SaveChangesAsync();
        }

        public async Task DeleteBookAsync(Guid id)
        {
            Book book = applicationDbContext
                .Books
                .Where(book => book.Id == id)
                .FirstOrDefault();
            applicationDbContext.Books.Remove(book);
            await applicationDbContext.SaveChangesAsync();
        }

        public async Task<Paginate<Book>> GetAllBooksAsync(string isbn,
            string title,
            string publisher,
            int yearPublished,
            decimal price,
            int offset,
            int limit)
        {
            var booksObtained = applicationDbContext
                .Books
                .Where(book => book.ISBN.Contains(isbn))
                .Where(book => book.Title.Contains(title))
                .Where(book => book.PublisherName.Contains(publisher))
                .Where(book => book.PublishedYear >= yearPublished)
                .Where(book => book.Price >= price)
                ;
            Paginate<Book> page = new Paginate<Book>()
            {
                Content = booksObtained
                        .Skip(offset * limit)
                        .Take(limit)
                        .ToList(),
                Total = booksObtained.Count()
            };

            return await Task.FromResult(page);
        }

        public async Task<Book> GetBookByIdAsync(Guid id)
        {
            return await Task.FromResult(applicationDbContext
                .Books
                .Where(book => book.Id == id)
                .AsNoTracking()
                .FirstOrDefault());
        }

        public async Task UpdateBookAsync(Book book)
        {
            applicationDbContext.Books.Update(book);
            await applicationDbContext.SaveChangesAsync();
        }
    }
}
