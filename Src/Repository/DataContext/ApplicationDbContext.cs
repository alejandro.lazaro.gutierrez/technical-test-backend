﻿using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.DataContext
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.UseSqlite("Filename=..//Repository//library.db");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>()
                   .HasKey(book => book.Id);

            modelBuilder.Entity<Book>()
                   .Property(book => book.Id)
                   .HasColumnType("BLOB(16)");

            modelBuilder.Entity<Book>()
                    .Property(book => book.ISBN)
                    .HasColumnType("VARCHAR(40)")
                    .HasDefaultValue(0)
                    .IsRequired();
            modelBuilder.Entity<Book>()
                   .Property(book => book.PublisherName)
                   .HasColumnType("VARCHAR(100)")
                   .HasDefaultValue(0)
                   .IsRequired();

            modelBuilder.Entity<Book>()
                   .Property(book => book.Title)
                   .HasColumnType("VARCHAR(255)")
                   .HasDefaultValue(0)
                   .IsRequired();

            modelBuilder.Entity<Book>()
                   .Property(book => book.Subtitle)
                   .HasColumnType("VARCHAR(200)")
                   .HasDefaultValue(0)
                   .IsRequired();

            modelBuilder.Entity<Book>()
                   .Property(book => book.Price)
                   .HasColumnType("DECIMAL(10,5)")
                   .HasDefaultValue(0)
                   .IsRequired();
        }

    }
}
