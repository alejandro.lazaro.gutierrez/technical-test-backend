﻿using Domain;
using Domain.Abstractions;
using Domain.Format;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Services
{
    public class BookService : IBookService
    {
        private readonly IBookRepository bookRepository;

        public BookService(IBookRepository bookRepository)
        {
            this.bookRepository = bookRepository;
        }

        public async Task CreateBookAsync(Book book)
        {
            await bookRepository.CreateBookAsync(book);
        }

        public async Task DeleteBookAsync(Guid id)
        {
            await bookRepository.DeleteBookAsync(id);
        }

        public async Task<Paginate<Book>> GetAllBooksAsync(string isbn,
            string title,
            string publisher,
            int yearPublished,
            decimal price,
            int offset,
            int limit)
        {
            return await bookRepository.GetAllBooksAsync(isbn, title, publisher, yearPublished, price, offset, limit);
        }

        public async Task<Book> GetBookByIdAsync(Guid id)
        {
            return await bookRepository.GetBookByIdAsync(id);
        }

        public async Task UpdateBookAsync(Book book)
        {
            await bookRepository.UpdateBookAsync(book);
        }
    }
}
