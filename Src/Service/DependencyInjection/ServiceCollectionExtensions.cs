﻿using Domain.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using Service.Services;

namespace Service.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IBookService, BookService>();
            return services;
        }
    }
}
